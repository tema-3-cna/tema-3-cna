﻿using Grpc.Core;
using Grpc.Net.Client;
using Server;
using Server.Protos;
using System;
using System.Threading.Tasks;
namespace Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("http://localhost:5001");

            var client = new Communication.CommunicationClient(channel);

            Console.WriteLine("Introduceti un nume: ");

            var name = Console.ReadLine();
            //str.GetString();
         
            Console.WriteLine(name);
            var user = new User
            {
                Id = Guid.NewGuid().ToString(),
                Name = name
            };

            ClientJoinRequest clientJoinRequest = new ClientJoinRequest { UserInfo = user };

           ClientJoinReply clientJoinReply = await client.ClientJoinAsync(clientJoinRequest);

            using (var msg = client.Transmitter())
            {
                var response = Task.Run(async () =>
                {
                    while (await msg.ResponseStream.MoveNext())
                    {

                        Console.Write($"{msg.ResponseStream.Current.ClientName}: {msg.ResponseStream.Current.Content};");

                    }
                });

                await msg.RequestStream.WriteAsync(new Message
                {
                    ClientId = user.Id,
                    ClientName = user.Name,
                    Content = ""
                });

                var message = Console.ReadLine();

                while (true)
                {
                    if (message != "")
                    {
                        Message message2 = new Message
                        {
                            ClientId = user.Id,
                            ClientName = user.Name,
                            Content = message,
                            Time = DateTime.Now.ToString()
                        };

                        await msg.RequestStream.WriteAsync(message2);
                    }

                    if (message.ToLower().Equals("exit")) { break; }

                    message = Console.ReadLine();
                }
                await msg.RequestStream.CompleteAsync();
            }
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();

        }
    }


}


