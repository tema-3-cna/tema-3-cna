﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class CommunicationService : Communication.CommunicationBase
    {
        private readonly ILogger<CommunicationService> logger;
        private static ChatGroupService chatGroupService = new ChatGroupService();
        public CommunicationService(ILogger<CommunicationService> log)
        {
            logger = log;
        }

        public override Task<ClientJoinReply> ClientJoin(ClientJoinRequest request, ServerCallContext context)
        {
            chatGroupService.AddClientToChatGroupAsync(request.UserInfo);
            return Task.FromResult(new ClientJoinReply { });
        }

        public override async Task Transmitter(Grpc.Core.IAsyncStreamReader<Message> requestStream,
    Grpc.Core.IServerStreamWriter<Message> responseStream,
    Grpc.Core.ServerCallContext context)
        {
            var info = context.GetHttpContext();

            logger.LogInformation($"Connection information: {info.Connection.Id}");
            //Sau requestStream.Current.Id

            chatGroupService.ConnectClientToChatGroup(Guid.Parse(requestStream.Current.ClientId), responseStream);

            while (await requestStream.MoveNext())
            {
                var userMessage = requestStream.Current;

                foreach (var client in chatGroupService.chatGroup.Clients)
                {
                    if (requestStream.Current==null)
                    {
                        chatGroupService.DisconnectClient(Guid.Parse(requestStream.Current.ClientId));
                        break;
                    }

                    await client.Stream.WriteAsync(userMessage);
                }
            }

            }
    }
}
