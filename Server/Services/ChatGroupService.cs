﻿using Grpc.Core;
using Server.Models;
using Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class ChatGroupService
    {
		public ChatGroup chatGroup;

		public ChatGroupService()
		{
			chatGroup = new ChatGroup();
		}

		public void AddClientToChatGroupAsync(User user)
		{
			chatGroup.Clients.Add(new Client
			{
				ID = Guid.Parse(user.Id),
				Name = user.Name
			});
		}

		public void ConnectClientToChatGroup(Guid customerId, IAsyncStreamWriter<Message> responseStream)
		{
			chatGroup.Clients.FirstOrDefault(c => c.ID == customerId).Stream = responseStream;
		}

		public void DisconnectClient(Guid userId)
		{
			chatGroup.Clients.Remove(chatGroup.Clients.FirstOrDefault(c => c.ID == userId));
		}
	}
}
