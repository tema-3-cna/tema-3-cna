﻿using Grpc.Core;
using Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models
{
    public class Client
    {
        private Guid m_ID;
        private String name;
        public IAsyncStreamWriter<Message> stream;

        public Client()
        {
            name = String.Empty;
        }

        public Guid ID
        {
            get
            {
                return this.m_ID;
            }
            set
            {
                this.m_ID = value;
            }
        }

        public String Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public IAsyncStreamWriter<Message> Stream
        {
            get
            {
                return this.stream;
            }
            set
            {
                this.stream = value;
            }
        }
    }
}
