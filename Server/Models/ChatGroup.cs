﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models
{
    public class ChatGroup
    {
        private List<Client> clients;

        public ChatGroup()
        {
            clients = new List<Client>();
        }

        public List<Client> Clients
        {
            get
            {
                return clients;
            }
        }
    }
}
