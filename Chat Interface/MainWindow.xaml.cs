﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat_Interface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            TxtName.Focus();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtValue.Text))
                return;

            if (EditString(TxtValue.Text) == "italic")
            {
                txtBlock.Text = TxtName.Text + ": ";
                txtBlock.Inlines.Add(new Italic(new Run(TxtValue.Text)));
                txtBlock.Inlines.Add(new LineBreak());
            }

            if (EditString(TxtValue.Text) == "bold")
            {
                txtBlock.Text = TxtName.Text + ": ";
                txtBlock.Inlines.Add(new Bold(new Run(TxtValue.Text)));
                txtBlock.Inlines.Add(new LineBreak());
            }

            if (EditString(TxtValue.Text) == "cut")
            {
                txtBlock.Text = TxtName.Text + ": ";
                txtBlock.Inlines.Add(new Underline(new Run(TxtValue.Text)));
                txtBlock.Inlines.Add(new LineBreak());
            }
            
            if (EditString(TxtValue.Text) == "underline")
            {
                
                txtBlock.Inlines.Add(new Underline(new Run(TxtValue.Text)));
                txtBlock.Inlines.Add(new LineBreak());
            }
            if (EditString(TxtValue.Text) == "normal")
            {
                txtBlock.Text += TxtName.Text + ": " + TxtValue.Text;
                txtBlock.Inlines.Add(new LineBreak());
            }

   
           
          
            TxtValue.Clear();
            TxtValue.Focus();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            String sir;
            sir = Console.ReadLine();

        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        public void AddString()
        {

        }

        static string EditString(string message)
        {
            string newMessage;
            int m = 0;
            string a = message;
            string aux = "";
            for (int i = 0; i < a.Length - 1; i++)
            {
                if (a.Substring(i, 2) == " _")
                {
                    i++;
                }
                if (a.Substring(i, 2) == "_ ")
                {
                    aux = a.Substring(m, i - m);
                    return "italic";
                }

            }

            for (int i = 0; i < a.Length - 1; i++)
            {
                if (a.Substring(i, 2) == " *")
                {
                    i++;
                }
                if (a.Substring(i, 2) == "* ")
                {
                    aux = a.Substring(m, i - m);
                    return "bold";
                }

            }

            for (int i = 0; i < a.Length - 1; i++)
            {
                if (a.Substring(i, 2) == " ~")
                {
                    i++;
                }
                if (a.Substring(i, 2) == "~ ")
                {
                    aux = a.Substring(m, i - m);
                    return "cut";
                }

            }

            for (int i = 0; i < a.Length - 1; i++)
            {
                if (a.Substring(i, 2) == " '")
                {
                    i++;
                }
                if (a.Substring(i, 2) == "' ")
                {
                    aux = a.Substring(m, i - m);
                    return "underline";
                }

            }
            return "normal";
        }

        private void TxtName_TextChanged(object sender, TextChangedEventArgs e)
        {
            String sir;
            sir = Console.ReadLine();
        }
    }
}
